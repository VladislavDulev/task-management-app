# Would you say your solution is robust? Why or why not?
    I would say that, given my experience, and this task being the first of its kind, that I've had to complete, I came up with a good solution that resulted in a functioning app. Naturally, there is always room for improvement and I'm sure that I could spend more hours on it and keep perfecting it.

# How would you improve your solution if you had more time?
    I guess my code could be structured in a cleaner way.
    Would improve field validations not to be as popup alert. 
    I also didn't manage to make the ID generation in increasing numbers. I am sure there is a very simple solution, but I didn't find a way to implement it. 

# How do you rate the complexity of this test task on the scale of 1 to 4 (4 being the most complex)?
    I would overall rate this task with a "3" for difficulty. It was mostly hard, because using Redux is a completely new thing for me and I had to learn as I do.
    I truly appreciated this task and had fun while figuring it out!